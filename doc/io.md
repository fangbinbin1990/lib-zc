# io

io. 源码见 src/stdlib/io.c

---


```
/* -1: 出错  0: 不可读写, 1: 可读写或socket异常  */
int zrwable(int fd);
int zreadable(int fd);
int zwriteable(int fd);

/* 设置fd非阻塞, 或 阻塞 */
int znonblocking(int fd, int no);

/* 设置 close_on_exec */
int zclose_on_exec(int fd, int on);

/* 检查fd有多少可读字节 */
int zget_readable_count(int fd);

/* 下面这些, 忽略信号EINTR的封装 */
int zopen(const char *pathname, int flags, mode_t mode);
ssize_t zread(int fd, void *buf, size_t count);
ssize_t zwrite(int fd, const void *buf, size_t count);
int zclose(int fd);
int zflock(int fd, int operation);
int zflock_share(int fd);
int zflock_exclusive(int fd);
int zfunlock(int fd);
int zrename(const char *oldpath, const char *newpath);
int zunlink(const char *pathname);
```
