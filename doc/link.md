# zlink\_t 

link是数据结构, 不是容器, (没有内存分配). link的实现借鉴了postfix和Linux内核的思想.

基于zlink\_t 可实现 [zlist\_t](./list.md), 队列, 栈等.

---

## struct
~~~
typedef struct zlink_node_t zlink_node_t;
typedef struct zlink_t zlink_t;
struct zlink_t {
    zlink_node_t *head;
    zlink_node_t *tail;
};
struct zlink_node_t {
    zlink_node_t *prev;
    zlink_node_t *next;
};

/* 初始化link指向的指针 */
void zlink_init(zlink_t *link);

/* 把node插到before前 */
zlink_node_t *zlink_attach_before(zlink_t *link, zlink_node_t *node, zlink_node_t *before);

/* 把节点node从link中移除 */
zlink_node_t *zlink_detach(zlink_t *link, zlink_node_t *node);

/* 把节点追加到尾部 */
zlink_node_t *zlink_push(zlink_t *link, zlink_node_t *node);

/* 把尾部节点弹出并返回 */
zlink_node_t *zlink_pop(zlink_t *link);

/* 把首部节点弹出并返回 */
zlink_node_t *zlink_unshift(zlink_t *link, zlink_node_t *node);

/* 把节点追加到首部 */
zlink_node_t *zlink_shift(zlink_t * link);

/* 返回首部节点 */
#define zlink_head(link)          ((link)->head)

/* 返回尾部节点 */
#define zlink_tail(link)          ((link)->tail)

/* 前一个节点 */
#define zlink_node_prev(node)     ((node)->prev)
/* 后一个节点 */
#define zlink_node_next(node)     ((node)->next)
```

## 例子

```
/* 记录姓名和年龄 */
zlink_t age_list; /\* zlink\_init(&age_list); \*/

typedef struct age_info age_info;
struct age_info {
    char *name;
    int age;
    zlink_node_t node;
};

age_info * add_one(char *name, int age)
{
  age_info *ai = (age_info *)malloc(sizeof(age_info));
  ai->name = strdup(name);
  ai->age = age;
  zlink_push(&age_list, &(ai->node));
  return ai;
}

```
