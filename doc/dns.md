## DNS
域名,ip相关. 源码见 src/stdlib/dns.c

mac地址. 源码见 src/stdlib/mac_address.c


---

```
/* 获取本机ip地址, 返回ip地址个数 */
int zget_localaddr(zargv_t *addrs);

/* 获取host对应的ip地址, 返回ip地址个数 */
int zget_hostaddr(const char *host, zargv_t *addrs);

/* 获取socket文件描述符sockfd,另一端的ip和端口信息; host: struct in_addr */
zbool_t zget_peername(int sockfd, int *host, int *port);

/* ip(struct in_addr)转ip地址(1.1.1.1), 结果存储在ipstr, 并返回 */
char *zget_ipstring(int ip, char *ipstr);

/* zget_ipstring 的反操作 */
int zget_ipint(const char *ipstr);

/* 返回ip的网络地址, masklen是掩码长度(下同) */
int zget_network(int ip, int masklen);

/* 返回子网掩码 */
int zget_netmask(int masklen);

/* 返回ip的广播地址 */
int zget_broadcast(int ip, int masklen);

/* 返回ip所在网段的最小地址 */
int zget_ip_min(int ip, int masklen);

/* 返回ip所在网段的最大地址 */
int zget_ip_max(int ip, int masklen);

/* 是否保留地址 */
int zip_is_intranet(int ip);

/* 是否保留地址 */
int zip_is_intranet2(const char *ip);

```

---

```
/* 获取mac地址; 返回个数, -1: 错; src/stdlib/mac_address.c */
int zget_mac_address(zargv_t *mac_list);
```

