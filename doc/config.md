# 配置, config

LIB-ZC采用了统一的配置风格. 不强制使用.  源码见 src/stdlib/config.c

---

```
#define zconfig_t zdict_t

/* 一个简单的通用配置文件风格
 * 推荐使用, 不强制使用. 一些内嵌服务和master/server使用此配置风格 
 * 行首第一个非空字符是#, 则忽略本行
 * 每配置行以 "=" 为分隔符
 * 配置项和配置值都需要过滤掉两侧的空白
 * 不支持任何转义
 * 相同配置项, 以后一个为准
 */

extern zconfig_t *zvar_default_config;
zconfig_t *zdefault_config_init(void);
void zdefault_config_fini(void);

#define zconfig_create  zdict_create
#define zconfig_free    zdict_free
#define zconfig_update  zdict_update
#define zconfig_update_string  zdict_update_string
#define zconfig_delete   zdict_delete
#define zconfig_debug_show    zdict_debug_show

/* 从文件pathname加载配置到cf, 同名则覆盖 */
int zconfig_load_from_pathname(zconfig_t *cf, const char *pathname);

/* 从配置another中加载配置到cf, 同名则覆盖 */
void zconfig_load_annother(zconfig_t *cf, zconfig_t *another);

/* 快速处理大批配置 */
typedef struct {
    const char *name;
    const char *defval;
    const char **target;
} zconfig_str_table_t;
typedef struct {
    const char *name;
    int defval;
    int *target;
    int min;
    int max;
} zconfig_int_table_t;
typedef struct {
    const char *name;
    long defval;
    long *target;
    long min;
    long max;
} zconfig_long_table_t;
typedef struct {
    const char *name;
    int defval;
    int *target;
} zconfig_bool_table_t;
#define zconfig_second_table_t  zconfig_long_table_t
#define zconfig_size_table_t    zconfig_long_table_t
#define zconfig_get_str         zdict_get_str
#define zconfig_get_bool        zdict_get_bool
#define zconfig_get_int         zdict_get_int
#define zconfig_get_long        zdict_get_long
#define zconfig_get_second      zdict_get_second
#define zconfig_get_size        zdict_get_size
void zconfig_get_str_table(zconfig_t *cf, zconfig_str_table_t *table);
void zconfig_get_int_table(zconfig_t *cf, zconfig_int_table_t *table);
void zconfig_get_long_table(zconfig_t *cf, zconfig_long_table_t *table);
void zconfig_get_bool_table(zconfig_t *cf, zconfig_bool_table_t *table);
void zconfig_get_second_table(zconfig_t *cf, zconfig_second_table_t *table);
void zconfig_get_size_table(zconfig_t *cf, zconfig_size_table_t *table);

/* 宏, 遍历. zconfig_t *cf; char *key; zbuf_t *value;  */
#define ZCONFIG_WALK_BEGIN(cf, key, value) { \
    zdict_node_t *___nd; char *key; zbuf_t *value; \
    for (___nd = zdict_first(cf);___nd;___nd=zdict_next(___nd)) { \
        key = zdict_node_key(___nd); value = zdict_node_value(___nd); {
#define ZCONFIG_WALK_END }}}
```

---

### 例子
见源码 sample/stdlib/config.c
