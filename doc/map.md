# zmap\_t 

map 实现. 源码见 src/stdlib/dict.c, 基于 [rbtree](./rbtree.md) 实现. 类似词典([dict](./dict.md)), 只不过值是指针

---

```
typedef struct zmap_node_t zmap_node_t;
typedef struct zmap_t zmap_t;

struct zmap_t {
    zrbtree_t rbtree;
    int len;
};
struct zmap_node_t {
    char *key; /* 键 */
    void *value; /* 值 */
    zrbtree_node_t rbnode;
};

/* 创建 */
zmap_t *zmap_create(void);
void zmap_free(zmap_t *map);

/* 重置 */
void zmap_reset(zmap_t *map);

/* 新增或更新节点并返回, 这个节点的键为key, 新值为value, 如果旧值存在则赋值给 *old_value */
zmap_node_t *zmap_update(zmap_t *map, const char *key, const void *value, void **old_value);

/* 查找键为key的节点,并返回. 如果存在则节点的值赋值给 *value */
zmap_node_t *zmap_find(const zmap_t *map, const char *key, void **value);

/* 查找键值小于key且最接近key的节点, 并... */
zmap_node_t *zmap_find_near_prev(const zmap_t *map, const char *key, void **value);

/* 查找键值大于key且最接近key的节点, 并... */
zmap_node_t *zmap_find_near_next(const zmap_t *map, const char *key, void **value);

/* 删除并释放键为key的节点, 节点的值赋值给 *old_value */
zbool_t zmap_delete(zmap_t * map, const char *key, void **old_value);

/* 移除节点n, 节点的值赋值给 *old_value */
void zmap_delete_node(zmap_t *map, zmap_node_t *n, void **old_value);

/* 更新节点的值, 节点的旧值赋值给 *old_value */
void zmap_node_update(zmap_node_t *n, const void *value, void **old_value);

/* 第一个 */
zmap_node_t *zmap_first(const zmap_t *map);

/* 最后一个 */
zmap_node_t *zmap_last(const zmap_t *map);

/* 前一个 */
zmap_node_t *zmap_prev(const zmap_node_t *node);

/* 后一个 */
zmap_node_t *zmap_next(const zmap_node_t *node);

/* 节点个数, map的长度 */
#define zmap_len(map)                 ((map)->len)

/* 节点的键 */
#define zmap_node_key(n)              ((n)->key)

/* 节点的值 */
#define zmap_node_value(n)            ((n)->value)

/* 宏, 遍历1 */
#define ZMAP_NODE_WALK_BEGIN(map, var_your_node)  { \
    zmap_node_t *var_your_node; \
    for(var_your_node = zmap_first(map); var_your_node; var_your_node = zmap_next(var_your_node)) {
#define ZMAP_NODE_WALK_END    }}

/* 宏, 遍历2 */
#define ZMAP_WALK_BEGIN(map, var_your_key, var_your_value_type, var_your_value)  { \
    zmap_node_t *var_your_node; char *var_your_key; var_your_value_type var_your_value; \
    (void)var_your_key; (void)var_your_value; \
    for(var_your_node = zmap_first(map); var_your_node; var_your_node = zmap_next(var_your_node)) { \
        var_your_key=var_your_node->key; var_your_value=(var_your_value_type)(void *)var_your_node->value; {
#define ZMAP_WALK_END    }}}

```

---

### 例子
见源码 sample/rbtree/map_account.c

