## 协程框架

支持协程, 文件io, 阻塞操作, 协程锁, 协程条件, 完整实现. 源码见 src/coroutine/

特别注意 ** 本协程不得跨线程操作 **


---

```
typedef struct zcoroutine_t zcoroutine_t;
typedef struct zcoroutine_mutex_t zcoroutine_mutex_t;
typedef struct zcoroutine_cond_t zcoroutine_cond_t;
/* 协程框架, 本协程不得跨线程操作 例子见 sample/coroutine/ */

/* 在线程内初始化协程基础环境框架 */
void zcoroutine_base_init();

/* 运行协程框架 */
void zcoroutine_base_run(void (*loop_fn)());

/* 通知当前协程环境退出, 既 zcoroutine_base_run() 返回 */
void zcoroutine_base_stop_notify();

/* 回收协程框架资源 */
void zcoroutine_base_fini();

/* 进入协程, 然后, 执行 start_job, 参数为ctx; stack_size: 协程栈大小, 建议不小于16K; */
void zcoroutine_go(void *(*start_job)(void *ctx), void *ctx, int stack_size);

/* 返回当前协程 */
zcoroutine_t * zcoroutine_self();

/* 放弃当前协程使用权 */
void zcoroutine_yield();

/* 主动结束协程 */
void zcoroutine_exit();

/* 睡眠 */
#define zcoroutine_sleep zsleep
#define zcoroutine_sleep_millisecond zsleep_millisecond

/* 获取当前协程上下文 */
void *zcoroutine_get_context();

/* 设置当前协程上下文 */
void zcoroutine_set_context(const void *ctx);

/* 主动设置fd支持协程切换 */
void zcoroutine_enable_fd(int fd);

/* 主动设置fd不支持协程切换 */
void zcoroutine_disable_fd(int fd);

/* 创建携程锁 */
zcoroutine_mutex_t * zcoroutine_mutex_create();
void zcoroutine_mutex_free(zcoroutine_mutex_t *);

/* 锁 */
void zcoroutine_mutex_lock(zcoroutine_mutex_t *);

/* 解锁 */
void zcoroutine_mutex_unlock(zcoroutine_mutex_t *);

/* 创建条件 */
zcoroutine_cond_t * zcoroutine_cond_create();
void zcoroutine_cond_free(zcoroutine_cond_t *);

/* 条件等待, 参考 pthread_cond_wait */
void zcoroutine_cond_wait(zcoroutine_cond_t *, zcoroutine_mutex_t *);

/* 条件信号, 参考 pthread_cond_signal */
void zcoroutine_cond_signal(zcoroutine_cond_t *);

/* 条件广播, 参考 pthread_cond_broadcast */
void zcoroutine_cond_broadcast(zcoroutine_cond_t *);

/* 启用limit个线程池, 用于文件io,和 block_do */
extern int zvar_coroutine_block_pthread_count_limit;

/* 如果
      zvar_coroutine_block_pthread_count_limit > 0 且
      zvar_coroutine_fileio_use_block_pthread == 1
   则 文件io在线程池执行, 否则在当前线程执行 */
extern zbool_t zvar_coroutine_fileio_use_block_pthread;

/* 如果 zvar_coroutine_block_pthread_count_limit > 0
   则 block_func(ctx) 在线程池执行, 否则在当前程直接执行 */
void *zcoroutine_block_do(void *(*block_func)(void *ctx), void *ctx);
```

---

```
/* zcoroutine_block_XXX 基于 zcoroutine_block_do 机制 */
int zcoroutine_block_pwrite(int fd, const void *data, int len, long offset);
int zcoroutine_block_write(int fd, const void *data, int len);
long zcoroutine_block_lseek(int fd, long offset, int whence);
int zcoroutine_block_open(const char *pathname, int flags, mode_t mode);
int zcoroutine_block_close(int fd);
int zcoroutine_block_rename(const char *oldpath, const char *newpath);
int zcoroutine_block_unlink(const char *pathname);

/* io 映射 */
void zcoroutine_go_iopipe(int fd1, SSL *ssl1, int fd2, SSL *ssl2, void (*after_close)(void *ctx), void *ctx);

```

---

### 例子
见源码 sample/coroutine/

