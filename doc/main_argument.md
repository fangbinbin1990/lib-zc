# 命令行参数

LIB-ZC大部分情况采用如下风格的参数, 不强制使用,但内嵌的服务,命令采用此风格
```
./cmd -name1 val1 arg1 -name2 val2 --bool1 --bool2 ... arg2 arg3
```
源码见 src/stdlib/main_argument.c

---

```
extern char *zvar_progname;
extern zbool_t zvar_proc_stop;
extern zbool_t zvar_test_mode;
extern int zvar_max_fd;

extern char **zvar_main_redundant_argv;
extern int zvar_main_redundant_argc;
/* 处理 main函数 argc,argv, 和 config无缝结合, 很方便. 默认参数风格如下:
 * ./cmd -name1 val1 arg1 -name2 val2 --bool1 --bool2 ... arg2 arg3
   执行 zmain_argument_run(argc, argv, 0) 后, 会自动创建一个全局配置文件
   zconfigt_t *zvar_default_config;
  
 * 而且, 逻辑上
   zvar_default_config[name1] == val1
   zvar_default_config[name2] == val2
   zvar_default_config[bool1] == yes
   zvar_default_config[bool2] == yes
   zvar_main_redundant_argc == 3
   zvar_main_redundant_argv[0] == argv1
   zvar_main_redundant_argv[1] == argv2
   zvar_main_redundant_argv[3] == argv3

 * main_argument_run 最后处理
   如果 zconfig_get_bool(zvar_default_config, "debug", 0) == 1, 则 zvar_log_debug_enable = 1
   如果 zconfig_get_bool(zvar_default_config, "fatal-catch", 0) == 1, 则 zvar_log_fatal_catch = 1

 * 如果参数项是 -config somepath.cf , 则
   会立即加载配置文件somepath.cf到zvar_default_config
   后加载的覆盖先加载的配置项, 命令行上的覆盖配置文件中的配置项
 */
void zmain_argument_run(int argc, char **argv, unsigned int (*self_argument_fn)(int argc, char **argv, int offset));
```
