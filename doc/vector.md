# zvector\_t 

一列指针, 源码见 src/stdlib/vector.c

---

```
/* 推荐使用 zvector_push, zvector_pop */
/* 不推荐使用 zvector_unshift, zvector_shift, zvector_insert, zvector_delete */

typedef struct zvector_t zvector_t;
struct zvector_t {
    char **data;
    int len;
    int size;
    int offset:31;
    unsigned int mpool_used:1;
};

#define zvector_data(v)        ((v)->data)
#define zvector_len(v)         ((v)->len)

/* 创建vector, 初始容量为size */
zvector_t *zvector_create(int size);
void zvector_free(zvector_t *v);

#define zvector_add zvector_push
/* 追加一个指针val到v的尾部 */
void zvector_push(zvector_t *v, const void *val);

/* 弹出尾部指针并赋值给*val, 存在则返回1, 否则返回 0 */
zbool_t zvector_pop(zvector_t *v, void **val);

/* 追加一个指针val到v的首部 */
void zvector_unshift(zvector_t *v, const void *val);

/* 弹出首部指针并赋值给*val, 存在则返回1, 否则返回 0 */
zbool_t zvector_shift(zvector_t *v, void **val);

/* 把val插到idx处, 远idx及其后元素顺序后移 */
void zvector_insert(zvector_t *v, int idx, void *val);

/* 弹出idx处的指针并赋值给*val, 存在则返回1, 否则返回 0 */
zbool_t zvector_delete(zvector_t *v, int idx, void **val);

/* 重置 */
void zvector_reset(zvector_t *v);

/* 截短到 new_len */
void zvector_truncate(zvector_t *v, int new_len);

/* 宏,遍历 */
#define ZVECTOR_WALK_BEGIN(arr, you_chp_type, var_your_ptr)    {\
    int  zvector_tmpvar_i; you_chp_type var_your_ptr;\
    for(zvector_tmpvar_i=0;zvector_tmpvar_i<(arr)->len;zvector_tmpvar_i++){\
        var_your_ptr = (you_chp_type)((arr)->data[zvector_tmpvar_i]);
#define ZVECTOR_WALK_END                }}

void zbuf_vector_reset(zvector_t *v);
void zbuf_vector_free(zvector_t *v);

```

---
### 例子1
见源码 sample/stdlib/vector.c


### 例子2
```
    zvector_t *vec = zvector_create(-1);
    zvector_push(vec, "1");
    zvector_push(vec, "2");
    zvector_push(vec, "3");

    char *str;
    zvector_pop(vec, (void **)&str); /* 返回1, str 指向 "3" */
    zvector_pop(vec, 0);             /* 返回1 */
    zvector_pop(vec, (void **)&str); /* 返回1, str 指向 "1" */

    zvector_pop(vec, (void **)&str); /* 返回0 */

    zvector_free(vec);

```
