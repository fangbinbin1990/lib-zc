# 简介

LIB-ZC是一个通用的Linux平台C扩展库.


## 背景

在Linux平台, 仅基于C标准库, 开发一个全新的完整的邮件系统, 包括

* 高并发smtp服务器
* 高并发imap/pop服务器
* 高并发http服务器(webmail, 管理)
* 反垃圾邮件网关, 基于贝叶斯的垃圾邮件识别系统
* 邮件解析, json, redis
* 等等

为此开发了 LIB-ZC 作为基础库

## 命名约定

* 函数, 结构体, 变量, 宏等 以字母 **z** 或 **Z** 开始
* 结构体以 **\_t** 结尾

## 安装

* 下载源代码, 解开并进入.
* make 
* 得到 libzc.a 和 libzc_coroutine.a

## 使用

* 源码 sample/下有大量例子,可供参考
* zc.h 和 libzc.a 在源码目录下

```
$ cat a.c
#include "zc.h"
int main(int argc, char **argv)
{
  /* foo(); */
  return 0;
}

$ gcc a.c ./libzc.a 
```

## socket, 连接, 监听地址
* "local:domain_socket_somepath", 等价于, "domain_socket_somepath"
* "fifo:somepath"
* "domain_socket_somepath"
* "somedomain:port"

## 时间/超时
* 如无特别说明, 所有的时间单位都是秒
* 如果是毫秒, 函数名或形参会明确提示
