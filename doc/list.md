# zlist\_t 

双向链表. 源码见 src/stdlib/list.c

---


```
typedef struct zlist_t zlist_t;

struct zlist_t {
    zlist_node_t *head;
    zlist_node_t *tail;
    int len;
};
struct zlist_node_t {
    zlist_node_t *prev;
    zlist_node_t *next;
    char *value;
};
#define zlist_head(c)   ((c)->head)
#define zlist_tail(c)   ((c)->tail)
#define zlist_len(c)    ((c)->len)
#define zlist_node_next(n)   ((n)->next)
#define zlist_node_prev(n)   ((n)->prev)
#define zlist_node_value(n)  ((n)->value)

/* 创建链表 */
zlist_t *zlist_create(void);
void zlist_free(zlist_t *list);

void zlist_reset(zlist_t *list);

/* 把节点node插到before前 */
void zlist_attach_before(zlist_t *list, zlist_node_t *n, zlist_node_t *before);

/* 移除节点 n, 并没有释放n的资源 */
void zlist_detach(zlist_t *list, zlist_node_t *n);

/* 创建一个值为value的节点,插到before前, 并返回 */
zlist_node_t *zlist_add_before(zlist_t *list, const void *value, zlist_node_t *before);

/* 删除节点n, 把n的值赋值给*value, 释放n的资源, 如果n==0返回0, 否则返回1 */
zbool_t zlist_delete(zlist_t *list, zlist_node_t *n, void **value);

/* 创建值为v的节点, 追加到链表尾部, 并返回 */
zinline zlist_node_t *zlist_push(zlist_t *l,const void *v){return zlist_add_before(l,v,0);}

/* 创建值为v的节点, 追加到链表首部部, 并返回 */
zinline zlist_node_t *zlist_unshift(zlist_t *l,const void *v){return zlist_add_before(l,v,l->head);}

/* 弹出尾部节点, 把值赋值给*v, 释放这个节点, 如果存在则返回1, 否则返回 0 */
zinline zbool_t zlist_pop(zlist_t *l, void **v){return zlist_delete(l,l->tail,v);}

/* 弹出首部节点, 把值赋值给*v, 释放这个节点, 如果存在则返回1, 否则返回 0 */
zinline zbool_t zlist_shift(zlist_t *l, void **v){return zlist_delete(l,l->head,v);}

/* 宏, 遍历 */
#define ZLIST_WALK_BEGIN(list, var_your_type, var_your_ptr)  { \
    zlist_node_t *list_current_node=(list)->head; var_your_type var_your_ptr; \
    for(;list_current_node;list_current_node=list_current_node->next){ \
        var_your_ptr = (var_your_type)(void *)(list_current_node->value);
#define ZLIST_WALK_END                          }}

/* 宏, 遍历 */
#define ZLIST_NODE_WALK_BEGIN(list, var_your_node)  { \
    zlist_node_t *var_your_node=(list)->head;\
    for(;var_your_node;var_your_node=var_your_node->next){
#define ZLIST_NODE_WALK_END                          }}

```

---

### 例子
见源码 sample/stdlib/list.c

