# zdict\_t 

词典实现. 源码见 src/stdlib/dict.c.  基于 [rbtree](./rbtree.md) 实现


---

```
typedef struct zdict_t zdict_t;
typedef struct zdict_node_t zdict_node_t;

struct zdict_t {
    zrbtree_t rbtree;
    int len; /* 节点个数 */
};
struct zdict_node_t {
    zrbtree_node_t rbnode;
    zbuf_t value; /* 值 */
    char *key; /* 键 */
};

/* 创建词典 */
zdict_t *zdict_create(void);
void zdict_free(zdict_t *dict);

/* 重置 */
void zdict_reset(zdict_t *dict);

/* 增加或更新节点,并返回此节点.  此节点键为key, 值为 dup_foo(value) */
zdict_node_t *zdict_update(zdict_t *dict, const char *key, const zbuf_t *value);

/* 增加或更新节点,并返回此节点.  此节点键为key, 值为(len<0?strdup(value):strndup(value, len)) */
zdict_node_t *zdict_update_string(zdict_t *dict, const char *key, const char *value, int len);

/* 查找键为key的节点,并返回. 如果存在则节点的值赋值给 *value */
zdict_node_t *zdict_find(const zdict_t *dict, const char *key, zbuf_t **value);

/* 查找键值小于key且最接近key的节点, 并... */
zdict_node_t *zdict_find_near_prev(const zdict_t *dict, const char *key, zbuf_t **value);

/* 查找键值大于key且最接近key的节点, 并... */
zdict_node_t *zdict_find_near_next(const zdict_t *dict, const char *key, zbuf_t **value);

/* 删除并释放键为key的节点 */
void zdict_delete(zdict_t *dict, const char *key);

/* 移除节点n */
void zdict_delete_node(zdict_t *dict, zdict_node_t *n);

/* 第一个节点 */
zdict_node_t *zdict_first(const zdict_t *dict);

/* 最后一个节点 */
zdict_node_t *zdict_last(const zdict_t *dict);

/* 前一个节点 */
zdict_node_t *zdict_prev(const zdict_node_t *node);

/* 后一个节点 */
zdict_node_t *zdict_next(const zdict_node_t *node);

/* 查找键为name的节点, 如果存在则返回其值, 否则返回def */
char *zdict_get_str(const zdict_t *dict, const char *name, const char *def);

/* 查找键为name的节点, 如果存在则返回zstr_to_bool(其值), 否则返回def */
int zdict_get_bool(const zdict_t *dict, const char *name, int def);

/* 查找键为name的节点, 如果存在且{ min < foo(其值) < max }则返回foo(其值), 否则返回def; foo 为 atoi */
int zdict_get_int(const zdict_t *dict, const char *name, int def, int min, int max);

/* 如上, foo 为 atol */
long zdict_get_long(const zdict_t *dict, const char *name, long def, long min, long max);

/* 如上, foo 为 zstr_to_second */
long zdict_get_second(const zdict_t *dict, const char *name, long def, long min, long max);

/* 如上, foo 为 zstr_to_size */
long zdict_get_size(const zdict_t *dict, const char *name, long def, long min, long max);

/* debug输出 */
void zdict_debug_show(const zdict_t *dict);

/* 个数 */
#define zdict_len(dict)                 ((dict)->len)

/* 节点的键 */
#define zdict_node_key(n)               ((n)->key)

/* 节点的值 */
#define zdict_node_value(n)             (&((n)->value))

/* 宏, 遍历1 */
#define ZDICT_WALK_BEGIN(dict, var_your_key, var_your_value)  { \
    zdict_node_t *var_your_node; char *var_your_key; zbuf_t *var_your_value; \
    for(var_your_node = zdict_first(dict); var_your_node; var_your_node = zdict_next(var_your_node)) { \
        var_your_key=var_your_node->key; var_your_value=&(var_your_node->value); {
#define ZDICT_WALK_END    }}}

/* 宏, 遍历1 */
#define ZDICT_NODE_WALK_BEGIN(dict, var_your_node)  { \
    zdict_node_t *var_your_node; \
    for(var_your_node = zdict_first(dict); var_your_node; var_your_node = zdict_next(var_your_node)) {
#define ZDICT_NODE_WALK_END    }}

```

---

### 例子
见源码 sample/rbtree/dict_account.c

---

## dictlong\_t
dictlong和dict的区别是值的类型是long
